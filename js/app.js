/********************************************************
 	Create an overlay-lightbox. Previous and next arrows
 *******************************************************/

var $overlay = $('<div id="overlay"></div>');// Create an overlay
var $imgArrows = $('<div></div>'); // div of img - arrows

var $image = $("<img>");
var $video = $("<div>");
var $previousArrow = $('<button class="previous-arrow"></button>');
var $nextArrow = $('<button class="next-arrow"></button>');
var $caption =$("<p></p>");

// Declare global variables of previous and next image links
var $nextImgLink;
var $previousImgLink;
// The player object of YouTube video
var player;

// Add the arrows, image & video to the div img-arrows
$imgArrows.append($previousArrow);
$imgArrows.append($image);
$imgArrows.append($video);
$imgArrows.append($nextArrow);
// Add the div img-arrows to the overlay
$overlay.append($imgArrows);
// Add caption to the overlay
$overlay.append($caption);
// Add overlay to the body
$("body").append($overlay);
// Hide the overlay
$overlay.hide();

// Capture the click event on a link to an image
$('#photo-gallery li a').click(function(event) {
	event.preventDefault();// Prevent to trigger the default action
	// Show the overlay
	showImageOnOverlay( $(this) );
});

$('.previous-arrow').click(function(event) {
	event.stopPropagation();// Prevent to trigger the overlay hide
	showImageOnOverlay( $previousImgLink );
});

$('.next-arrow').click(function(event) {
	event.stopPropagation();// Prevent to trigger the overlay hide
	showImageOnOverlay( $nextImgLink );
});

// Function showing the overlay with image inside & get previous, next images
function showImageOnOverlay(theImageLink) {
	// Get the href of the current image link
	var imageLocation = theImageLink.attr("href");
	// Check for image or video in the link
	if ( imageLocation ) {
		// For image
		showTheImage( imageLocation );		
	} else {
		// For video
		showTheVideo( theImageLink );
	}
	// Get the alt value and set the caption
	var captionText = theImageLink.children("img").attr("alt");
	$caption.text(captionText);
	// Show the overlay and img-arrows with slow gesture
	$overlay.show('400');
	//Update the previous and next images
	$nextImgLink = theImageLink.parent('li').nextAll('li').not('.hideClass').first().children('a');
	$previousImgLink = theImageLink.parent('li').prevAll('li').not('.hideClass').first().children('a');
	
	// Check if user reaches the last element then go to the first
	if ( $nextImgLink.length === 0 ) {
		$nextImgLink = $('#photo-gallery li').not('.hideClass').first().children('a');

	}
	// Check if user reaches the first element then go to the last
	if ( $previousImgLink.length === 0 ) {
		$previousImgLink = $('#photo-gallery li').not('.hideClass').last().children('a');
	}
}

//Hide the overlay when click on it
$overlay.click(function() {
	closeOverlay();
});

function closeOverlay() {
	$overlay.hide();
	$previousImgLink = null; // Unset the variable of previous link
	$nextImgLink = null; // Unset the variable of next link
	removePlayer();// Remove the player
}

/********************************************************
	The search functions. Keyboards navigations
 *******************************************************/

// Create a delay function (self-evoking)
var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

// Select the search input
var $searchInput = $('input[type="search"]');

// When ever key is up, trigger a search with a delay
$searchInput.keyup(function () {
	delay(function(){

	// Get the search input
	var searchVal = $searchInput.val();
	searchVal = searchVal.toLowerCase();

	var listOfLinks = $('#photo-gallery li');
	listOfLinks.each(function (index) {
		// Get the alt value of the image
		var captionAlt = $(this).children('a').children('img').attr('alt');
		captionAlt = captionAlt.toLowerCase();
		// Check if the search values match the captions
		var matchedArray = captionAlt.match( searchVal );
		if ( !matchedArray ) {
			// Hide the image without match
			if ( !$(this).hasClass('hideClass') ) {
				$(this).addClass('hideClass');
				$(this).fadeOut(1000);
			}
		} else {
			// Show the matched ones
			if ( $(this).hasClass('hideClass') ) {
				$(this).removeClass('hideClass');
				$(this).fadeIn(1000);
			}
		}
	});

    }, 500 );// 500 is the delay in ms
});

// Keyboard navigation for browsing photos
// Using the left arrow key and right arrow key
$("body").keydown(function (e) {
	if ( e.keyCode === 37 && $previousImgLink ) { // Left arrow key
		showImageOnOverlay( $previousImgLink );
	} else if ( e.keyCode === 39 && $nextImgLink ) { // Right arrow key
		showImageOnOverlay( $nextImgLink );
	} else if ( e.keyCode === 27 ) {
		closeOverlay();
	}
});

/********************************************************
	Additional media types of Youtube videos
 *******************************************************/

function showTheImage( imageSrc ) {
	removePlayer();
	// Hide the video div and show the image
	$video.hide();
	$image.show();
	// Update image source
	$image.attr("src", imageSrc);
}

function showTheVideo( currentLink ) {
	// Hide the image & show the video
	$image.hide();
	removePlayer();
	$video.show();
	// Assign the id 'player' to the div of video
	$video.attr('id', 'player');
	// Get the videoID from the thumbnail
	var imgSrcText = currentLink.children('img').attr('src');
	var videoID = imgSrcText.slice(26, 37);
	// Create the player object with the videoID
	player = createPlayer('player', videoID);
}

// This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// This function creates an <iframe> (and YouTube player)
// after the API code downloads.
function createPlayer(playerID, IDofVideo) {
	var playerObject = new YT.Player(playerID, {
		videoId: IDofVideo,
		playerVars: { 'controls': 0 },
		events: {
			'onReady' : onPlayerReady
		}
	});
	return playerObject;
}

function onPlayerReady(event) {
	event.target.playVideo();
	event.target.setPlaybackQuality( 'hd720' );
	event.target.setVolume(100);
}

// Check for the existance of 'iframe' to destroy the player
function removePlayer() {
	if ( $('iframe').length ) {
		player.destroy();
	}
}